TODO
====


Instalacja narzędzi
-------------------

* artifactory
* sputnik
* sonarqube for stash
* hipchat
* jira for jenkins (pokazywanie statusów buildów)


Konfiguracja
------------

* zrobić jenkins for jira (pokazywanie issuesów których dotyczy build)
* gitflow budowanie w jenkinsie zgodnie z konwencją nazewniczą
** różne buildy ze względu na branch
** smoke testy dla commitów
** cięższe dla pull requestów
** testy wydajnościowe dla Pull Requestów
* BDD
* Plan w Jenkinsie do uruchamiania budowania onDemand (podając hash lub branch)


Nowe projekty w SonarQube i Jenkins
-----------------------------------

* php
* python
* ruby
* closure
* scala


SonarQube
---------

* podpiąć projekty do sonara z języków pozyżej
* Pittest
* Quality Gates

