Install Stash
-------------

.. code-block:: bash

    wget https://www.atlassian.com/software/stash/downloads/binary/atlassian-stash-3.8.0-x64.bin
    chmod +x atlassian-stash-3.8.0-x64.bin
    ./atlassian-stash-3.8.0-x64.bin
    rm -fr atlassian-stash-3.8.0-x64.bin

